package mytest;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;
import top.topicsky.www.fitcore.global.util.impl.plugs.FitCoreBeanGetSetReflectImpl;
import top.topicsky.www.fitcore.procedure.service.impl.FitCoreExceptionServiceImpl;
import top.topicsky.www.fitcore.system.kernel.engine.consumer.FitCoreBasicConsumerInter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerAssertUtil.NOTNULL;
import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： mytest
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 22 日 10 时 41 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class MyTest{
    public static void main(String[] args){
        List<Object> beanObject=new ArrayList<>();
        FitCoreUserEntity fitCoreUserEntity=new FitCoreUserEntity();
        beanObject.add(fitCoreUserEntity);
        beanObject.add("fitCoreUuid");
        beanObject.add("12345");
        new FitCoreBeanGetSetReflectImpl().setProperty(beanObject);
        System.out.println(fitCoreUserEntity.getFitCoreUuid());

        //FitCoreCheckboxImpl fitCoreCheckboxImpl=new FitCoreCheckboxImpl();
        //Model model=new Model(){
        //    @Override
        //    public Model addAttribute(String s,Object o){
        //        return null;
        //    }
        //
        //    @Override
        //    public Model addAttribute(Object o){
        //        return null;
        //    }
        //
        //    @Override
        //    public Model addAllAttributes(Collection<?> collection){
        //        return null;
        //    }
        //
        //    @Override
        //    public Model addAllAttributes(Map<String,?> map){
        //        return null;
        //    }
        //
        //    @Override
        //    public Model mergeAttributes(Map<String,?> map){
        //        return null;
        //    }
        //
        //    @Override
        //    public boolean containsAttribute(String s){
        //        return false;
        //    }
        //
        //    @Override
        //    public Map<String,Object> asMap(){
        //        return null;
        //    }
        //};
        //List listchoice=new ArrayList<>();
        //listchoice.add("A");
        //listchoice.add("B");
        //listchoice.add("C");
        //String listchoiceName="fitCoreSpringTagsLiat";
        //FitCoreSpringTagsDto fitCoreSpringTagsDto=new FitCoreSpringTagsDto();
        //String objPropert="fitCoreSpringTagsBoolean";
        //Boolean objValue=true;
        //String choiceName="fitCoreSpringTagsDto";
        //String pageForword="index";
        //fitCoreCheckboxImpl.FitCoreCheckboxsPack(
        //        model,
        //        listchoice,
        //        listchoiceName,
        //        fitCoreSpringTagsDto,
        //        objPropert,
        //        objValue,
        //        choiceName,
        //        pageForword
        //);
        //System.out.println(fitCoreSpringTagsDto.getFitCoreSpringTagsBoolean());
        //fitCoreSpringTagsDto.getFitCoreSpringTagsLiat().stream().forEach(item ->System.out.println(item));
    }

    public void testMyCode(){
        ApplicationContext applicationContext = new  FileSystemXmlApplicationContext("classpath:top/topicsky/www/fitcore/procedure/service/spring-service-fitcore.xml");
        FitCoreExceptionServiceImpl fitCoreExceptionServiceImpl=(FitCoreExceptionServiceImpl)applicationContext.getBean("fitCoreExceptionServiceImpl");
        System.out.println(fitCoreExceptionServiceImpl);
    }

    public void testAssert(){
        //String a= "有值";
        //String b= "";
        //String c= null;
        //SOPL.accept("传入有值，什么也不做");
        //NOTNULL.accept(a);
        //SOPL.accept("传入非null，但是空，什么也不做");
        //NOTNULL.accept(b);
        //SOPL.accept("传入null，报断言");
        //NOTNULL.accept(c);
        //SOPL.accept("传入有值，什么也不做");
        //assert a!=null: "传入对象不存在";
        //SOPL.accept("传入非null，但是空，什么也不做");
        //assert b!=null: "传入对象不存在";
        //SOPL.accept("传入null，报断言");
        //assert c!=null: "传入对象不存在";
        //Optional
        SOPL.accept("兰剑");
        SOPL.accept(OTS.apply(123));
    }
}
