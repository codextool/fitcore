package top.topicsky.www.fitcore.global.community.multithreaded.threadpoolintermediateimplement;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.activemq.thread.Task;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.threadpoolimplement
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 15 时 41 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public abstract class FitCoreInterMediateThreadTaskImpl implements Runnable, Serializable{
    // private static Logger logger = Logger.getLogger(Task.class);
    /* 产生时间 */
    private Date generateTime=null;
    /* 提交执行时间 */
    private Date submitTime=null;
    /* 开始执行时间 */
    private Date beginExceuteTime=null;
    /* 执行完成时间 */
    private Date finishTime=null;
    /* 任务Id*/
    private long taskId;

    /**
     * Instantiates a new Fit core inter mediate thread task.
     */
    public FitCoreInterMediateThreadTaskImpl(){
        this.generateTime=new Date();
    }

    /**
     * 任务执行入口
     * <p>
     * 相关执行代码
     * <p>
     * beginTransaction();
     * <p>
     * 执行过程中可能产生新的任务 subtask = taskCore();
     * <p>
     * commitTransaction();
     * <p>
     * 增加新产生的任务 ThreadPool.getInstance().batchAddTask(taskCore());
     */
    public void run(){
        SOPL.accept("执行 任务 run()");
    }

    /**
     * 所有任务的核心 所以特别的业务逻辑执行之处
     *
     * @return the task [ ]
     *
     * @throws Exception
     *         the exception
     */
    public abstract Task[] taskCore() throws Exception;

    /**
     * 是否用到数据库
     *
     * @return boolean boolean
     */
    protected abstract boolean useDb();

    /**
     * 是否需要立即执行
     *
     * @return boolean boolean
     */
    protected abstract boolean needExecuteImmediate();

    /**
     * 任务信息
     *
     * @return String string
     */
    public abstract String info();
}
