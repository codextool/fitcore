package top.topicsky.www.fitcore.global.community.multithreaded.implementrunnable;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.engine.runnable.FitCoreBasicRunnableInter;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.implementrunnable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 09 时 54 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicRunnableImpl implements FitCoreBasicRunnableInter, Runnable, Serializable{
    private Integer integerx=0;

    @Override
    public void run(){
        SOPL.accept("这里开启一个新线程");
        for(int i=0;i<300;i++){
            synchronized(this){
                SOPL.accept(Thread.currentThread().getName()+":"+integerx);
                integerx++;
            }
        }
    }
}

