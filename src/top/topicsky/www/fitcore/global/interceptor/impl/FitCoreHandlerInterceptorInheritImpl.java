package top.topicsky.www.fitcore.global.interceptor.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import top.topicsky.www.fitcore.global.interceptor.inter.FitCoreHandlerInterceptorInheritInter;
import top.topicsky.www.fitcore.global.interceptor.predicate.FitCoreHandlerInterceptorInheritAbstractPredicateImpl;
import top.topicsky.www.fitcore.global.interceptor.predicate.FitCoreHandlerInterceptorInheritPredicateImpl;
import top.topicsky.www.fitcore.global.interceptor.process.FitCoreHandlerInterceptorInheritAbstractProcessImpl;
import top.topicsky.www.fitcore.global.interceptor.process.FitCoreHandlerInterceptorInheritProcessImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.interceptor.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 23 日 14 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public abstract class FitCoreHandlerInterceptorInheritImpl implements FitCoreHandlerInterceptorInheritInter, Serializable{
    private static final long serialVersionUID=5398776727356298661L;

    /**
     * Instantiates a new Fit core handler interceptor inherit.
     */
    public FitCoreHandlerInterceptorInheritImpl(){
    }

    /**
     * Fit core handler interceptor inherit init.
     */
    public void FitCoreHandlerInterceptorInheritInit(){
        FitCoreHandlerInterceptorInheritAbstractProcessImpl.FitCoreHandlerInterceptorAbstractInheritInit("initAbstract_Inherit",
                FitCoreHandlerInterceptorInheritAbstractPredicateImpl::FitCoreHandlerInterceptorAbstractInheritInit);
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o) throws Exception{
        Runnable runnable=()->FitCoreHandlerInterceptorInheritProcessImpl.FitCoreHandlerInterceptorInheritInit("preHandle_Inherit",
                FitCoreHandlerInterceptorInheritPredicateImpl::FitCoreHandlerInterceptorInheritInit);
        runnable.run();
         /*这里定义是否有效执行，布尔值*/
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o,ModelAndView modelAndView) throws Exception{
        Runnable runnable=()->FitCoreHandlerInterceptorInheritProcessImpl.FitCoreHandlerInterceptorInheritInit("postHandle_Inherit",
                FitCoreHandlerInterceptorInheritPredicateImpl::FitCoreHandlerInterceptorInheritInit);
        runnable.run();
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o,Exception e) throws Exception{
        Runnable runnable=()->FitCoreHandlerInterceptorInheritProcessImpl.FitCoreHandlerInterceptorInheritInit("afterCompletion_Inherit",
                FitCoreHandlerInterceptorInheritPredicateImpl::FitCoreHandlerInterceptorInheritInit);
        runnable.run();
    }
}
