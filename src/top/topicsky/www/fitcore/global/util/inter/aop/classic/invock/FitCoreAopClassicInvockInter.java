package top.topicsky.www.fitcore.global.util.inter.aop.classic.invock;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter.aop.classic.invock
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 08 日 14 时 53 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreAopClassicInvockInter{
    /**
     * Fit core aop classic impl invock proxy xa string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXa();

    /**
     * Fit core aop classic impl invock proxy xb string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXb();

    /**
     * Fit core aop classic impl invock proxy xc string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXc();

    /**
     * Fit core aop classic impl invock proxy xd string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXd();

    /**
     * Fit core aop classic impl invock proxy xe string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXe();

    /**
     * Fit core aop classic impl invock proxy xf string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXf();

    /**
     * Fit core aop classic impl invock proxy xg string.
     *
     * @return the string
     */
    String fitCoreAopClassicImplInvock_ProxyXg();
}
