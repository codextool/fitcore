package top.topicsky.www.fitcore.global.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Util con vert.
 */
@Component
@Transactional
public class UtilConVert implements Converter<String,Object>, Serializable{
    @Override
    public Object convert(String stringDate){
        try{
            Date date=new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
            return date;
        }catch(ParseException e){
            e.printStackTrace();
        }
        return null;
    }
}
