package top.topicsky.www.fitcore.global.util.impl.aop.aspectj.invock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import top.topicsky.www.fitcore.global.util.inter.aop.aspectj.FitCoreAopAspectjInter;
import top.topicsky.www.fitcore.global.util.inter.aop.aspectj.invock.FitCoreAopAspectjInvockInter;
import top.topicsky.www.fitcore.global.util.predicate.aop.aspectj.invock.FitCoreAopAspectjInvockPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.aop.aspectj.invock.FitCoreAopAspectjInvockProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.aspectj.invock
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 08 日 14 时 39 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
@RequestMapping("/fitcoreaop")
public class FitCoreAopAspectjInvockImpl implements FitCoreAopAspectjInvockInter, Serializable{
    /*#####################################注入控制层######################################################*/
    /*#####################################注入控制层######################################################*/
    /*#####################################注入业务层######################################################*/
    @Qualifier("fitCoreAopAspectjImplProxyXh")
    @Autowired
    private FitCoreAopAspectjInter fitCoreAopAspectjImplProxyXh;
    /*#####################################注入业务层######################################################*/
    /*#####################################注入持久层######################################################*/

    /*#####################################注入持久层######################################################*/
    /*#####################################注入数据层######################################################*/

    /*#####################################注入数据层######################################################*/
    /*#####################################依赖配置类######################################################*/

    /*#####################################依赖配置类######################################################*/
    /*#####################################自定义注解######################################################*/

    /*#####################################自定义注解######################################################*/

    @RequestMapping(value="/aspectjha", method=RequestMethod.GET)
    public String fitCoreAopAspectjImplInvock_ProxyXa(){
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjInvockProcessImpl.FitCoreAopAspectjInvockInit("Init Message",
                        FitCoreAopAspectjInvockPredicateImpl::FitCoreAopAspectjInvockInit)
        );
        runnable.run();
        String args1="args1";
        Integer args2=00000000;
        fitCoreAopAspectjImplProxyXh.fitCoreAopAspectjBeanInitProxyXa(args1,args2);
        return "MyCode";
    }
}
