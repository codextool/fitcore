package top.topicsky.www.fitcore.global.util.impl.plugs.generateexcel;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import top.topicsky.www.fitcore.global.util.inter.plugs.generateexcel.FitCoreGenerateExcelViewInter;
import top.topicsky.www.fitcore.global.util.predicate.plugs.generateexcel.FitCoreGenerateExcelViewProcessImpl;
import top.topicsky.www.fitcore.global.util.process.plugs.generateexcel.FitCoreGenerateExcelViewPredicateImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.plugs.generateexcel
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 17 日 13 时 06 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
@RequestMapping("/excelview")
public class FitCoreGenerateExcelViewImpl implements FitCoreGenerateExcelViewInter, Serializable{
    /**
     * Gets excel view request.
     *
     * @param httpServletRequest
     *         the http servlet request
     * @param httpServletResponse
     *         the http servlet response
     *
     * @return the excel view request
     *
     * @throws Exception
     *         the exception
     */
    @RequestMapping(value="/generateexcelview")
    public ModelAndView getExcelViewRequest(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) throws Exception{
        return new ModelAndView("fitCoreGenerateExcelProcessImplBean",
                "fitCoreLogInFormDtoDate",
                FitCoreGenerateExcelViewPredicateImpl.fitCoreGenerateExcelViewPredicateImplInit(httpServletRequest,
                        httpServletResponse,
                        FitCoreGenerateExcelViewProcessImpl::getFitCoreLogInFormDtoExcelList)
        );
    }
}
