package top.topicsky.www.fitcore.global.util.impl.aop.aspectj.advice;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.aop.aspectj.advice.FitCoreAopAspectjAdviceInter;
import top.topicsky.www.fitcore.global.util.predicate.aop.aspectj.advice.FitCoreAopAspectjAdvicePredicateImpl;
import top.topicsky.www.fitcore.global.util.process.aop.aspectj.advice.FitCoreAopAspectjAdviceProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.classic
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 07 日 19 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
/*
* @Aspect的注解来标识切面
* */
@Aspect
@Component
@Transactional
public class FitCoreAopAspectjAdviceImpl implements FitCoreAopAspectjAdviceInter, Serializable{
    /**
     * Instantiates a new Fit core aop aspectj advice.
     */
    public FitCoreAopAspectjAdviceImpl(){
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjAdviceProcessImpl.FitCoreAopAspectjAdviceInit("Init Message",
                        FitCoreAopAspectjAdvicePredicateImpl::FitCoreAopAspectjAdviceInit)
        );
        runnable.run();
    }

    /**
     * Pointcut
     * 定义Pointcut，Pointcut的名称为aspectjMethod()，此方法没有返回值和参数
     * 该方法就是一个标识，不进行调用
     *
     * @param args1
     *         the args 1
     * @param args2
     *         the args 2
     */
    @Pointcut("execution(* top.topicsky.www.fitcore.global.util.impl.aop.aspectj.FitCoreAopAspectjImpl.fitCoreAopAspectjBeanInitProxyXa(..)) && args(args1,args2)")
    public void fitCoreAopAspectjAdvicePointcut(String args1,Integer args2){
    }

    /**
     * Before
     * 在核心业务执行前执行，不能阻止核心业务的调用。
     * // * @param joinPoint
     */
    @Override
    @Before(value="fitCoreAopAspectjAdvicePointcut(args1,args2)", argNames="args1,args2")
    public void before(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjAdviceProcessImpl.FitCoreAopAspectjAdviceInit("Init Message",
                        FitCoreAopAspectjAdvicePredicateImpl::FitCoreAopAspectjAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"before");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    /**
     * Around
     * 手动控制调用核心业务逻辑，以及调用前和调用后的处理,
     * <p>
     * 注意：当核心业务抛异常后，立即退出，转向AfterAdvice
     * 执行完AfterAdvice，再转到ThrowingAdvice
     * // * @param pjp
     * // * @return
     * // * @throws Throwable
     */
    @Override
    @Around(value="fitCoreAopAspectjAdvicePointcut(args1,args2)", argNames="args1,args2")
    public void around(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjAdviceProcessImpl.FitCoreAopAspectjAdviceInit("Init Message",
                        FitCoreAopAspectjAdvicePredicateImpl::FitCoreAopAspectjAdviceInit)
        );
        runnable.run();
        System.out.println("环绕通知开始...");
        try{
            System.out.println("前面拦截....");
            System.out.println("args1:"+args1+",args2:"+args2+"around");
            System.out.println("后面拦截.....");
        }catch(Throwable error){
            System.out.println("环绕通知开失败...");
            try{
                throw error;
            }catch(Throwable throwable){
                throwable.printStackTrace();
            }
        }
        System.out.println("环绕通知结束...");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    /**
     * After
     * 核心业务逻辑退出后（包括正常执行结束和异常退出），执行此Advice
     * // * @param joinPoint
     */
    @Override
    @After(value="fitCoreAopAspectjAdvicePointcut(args1,args2)", argNames="args1,args2")
    public void after(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjAdviceProcessImpl.FitCoreAopAspectjAdviceInit("Init Message",
                        FitCoreAopAspectjAdvicePredicateImpl::FitCoreAopAspectjAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"after");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    /**
     * 核心业务逻辑调用异常退出后，执行此Advice，处理错误信息
     * <p>
     * 注意：执行顺序在Around Advice之后
     * // * @param joinPoint
     * // * @param ex
     */
    @Override
    @AfterThrowing(value="fitCoreAopAspectjAdvicePointcut(args1,args2)", argNames="args1,args2")
    public void afterThrowing(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjAdviceProcessImpl.FitCoreAopAspectjAdviceInit("Init Message",
                        FitCoreAopAspectjAdvicePredicateImpl::FitCoreAopAspectjAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"afterThrowing");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    /**
     * AfterReturning
     * 核心业务逻辑调用正常退出后，不管是否有返回值，正常退出后，均执行此Advice
     * 可以对返回值做进一步处理
     * // * @param joinPoint
     */
    @Override
    @AfterReturning(value="fitCoreAopAspectjAdvicePointcut(args1,args2)", argNames="args1,args2")
    public void afterReturning(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopAspectjAdviceProcessImpl.FitCoreAopAspectjAdviceInit("Init Message",
                        FitCoreAopAspectjAdvicePredicateImpl::FitCoreAopAspectjAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"afterReturning");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }
}
