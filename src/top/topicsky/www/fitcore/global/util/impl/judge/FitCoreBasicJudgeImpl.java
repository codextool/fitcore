package top.topicsky.www.fitcore.global.util.impl.judge;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.judge.FitCoreBasicJudgeInter;
import top.topicsky.www.fitcore.global.util.predicate.judge.FitCoreBasicJudgePredicateImpl;
import top.topicsky.www.fitcore.global.util.process.judge.FitCoreBasicJudgeProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.judge
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 13 时 47 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicJudgeImpl implements FitCoreBasicJudgeInter, Serializable{
    /**
     * Judge null boolean.
     *
     * @param o
     *         the o
     *
     * @return the boolean
     */
    public static Boolean judgeNull(Object o){
        return FitCoreBasicJudgeProcessImpl.judgeProcess(o,
                FitCoreBasicJudgePredicateImpl::judgeNullPredicate);
    }

    /**
     * Judge size zero boolean.
     *
     * @param o
     *         the o
     *
     * @return the boolean
     */
    public static Boolean judgeSizeZero(Object o){
        return FitCoreBasicJudgeProcessImpl.judgeProcess(o,
                FitCoreBasicJudgePredicateImpl::judgeSizeZeroPredicate);
    }

    /**
     * Judge length zero boolean.
     *
     * @param o
     *         the o
     *
     * @return the boolean
     */
    public static Boolean judgeLengthZero(Object o){
        return FitCoreBasicJudgeProcessImpl.judgeProcess(o,
                FitCoreBasicJudgePredicateImpl::judgeLengthZeroPredicate);
    }
}
