package top.topicsky.www.fitcore.global.util.impl.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.plugs.FitCoreBeanClassReflectInter;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.plugs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 08 日 11 时 31 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBeanClassReflectImpl implements FitCoreBeanClassReflectInter, Serializable{
    @Override
    public Object getFieldValueByName(String fieldName,Object object){
        try{
            String firstLetter=fieldName.substring(0,1).toUpperCase();
            String getter="get"+firstLetter+fieldName.substring(1);
            Method method=object.getClass().getMethod(getter,new Class[]{});
            Object value=method.invoke(object,new Object[]{});
            return value;
        }catch(Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public String[] getFiledName(Object object){
        Field[] fields=object.getClass().getDeclaredFields();
        String[] fieldNames=new String[fields.length];
        for(int i=0;i<fields.length;i++){
            fieldNames[i]=fields[i].getName();
        }
        return fieldNames;
    }

    @Override
    public List getFiledsInfo(Object object){
        Field[] fields=object.getClass().getDeclaredFields();
        String[] fieldNames=new String[fields.length];
        List list=new ArrayList();
        Map infoMap=null;
        for(int i=0;i<fields.length;i++){
            infoMap=new HashMap();
            infoMap.put("type",fields[i].getType().toString());
            infoMap.put("name",fields[i].getName());
            infoMap.put("value",getFieldValueByName(fields[i].getName(),object));
            list.add(infoMap);
        }
        return list;
    }

    @Override
    public Object[] getFiledValues(Object object){
        String[] fieldNames=this.getFiledName(object);
        Object[] value=new Object[fieldNames.length];
        for(int i=0;i<fieldNames.length;i++){
            value[i]=this.getFieldValueByName(fieldNames[i],object);
        }
        return value;
    }

    @Override
    public void setFieldToNull(Object object){
        //获取自己的属性
        Field[] fields1=object.getClass().getDeclaredFields();
        //获得父类的属性
        Field[] fields2=object.getClass().getSuperclass().getDeclaredFields();
        //合并所有属性
        Field[] fields=new Field[fields1.length+fields2.length];
        for(int i=0;i<fields1.length;++i){
            fields[i]=fields1[i];
        }
        for(int j=0;j<fields2.length;++j){
            fields[fields1.length+j]=fields2[j];
        }
        for(int i=0;i<fields.length;i++){
            String fieldName=fields[i].getName();  //属性名称
            String filedType=fields[i].getType().toString(); //属性类型
            //去掉前面的class啊inteface啊这些说明
            filedType=filedType.substring(filedType.indexOf(" ")+1);
            //如果是我自定义的类型的
            if(filedType.indexOf("com.shutao.test")!=-1||
                    filedType.indexOf("Set")!=-1){
                String firstLetter=fieldName.substring(0,1).toUpperCase();
                //得到方法名称
                String setter="set"+firstLetter+fieldName.substring(1);
                try{
                    //先调用自身的方法
                    Method callMethod=object.getClass().getMethod(setter,Class.forName(filedType));
                    callMethod.invoke(object,new Object[]{null});
                }catch(Exception e){
                    try{
                        //上面调用自身的方法如果调不到会抛异常，这里再调用父类的方法
                        Method callMethod=object.getClass().getSuperclass().getDeclaredMethod(setter,Class.forName(filedType));
                        callMethod.invoke(object,new Object[]{null});
                    }catch(Exception e1){
                        System.out.println(e1.getMessage());
                    }
                }
            }
        }
    }

    @Override
    public void callGetMethodForSetField(Object object){
        /*Field[] fields = getFieldsIncludeParent(object);
        for(int i=0;i<fields.length;i++){
            String fieldName = fields[i].getName();  //属性名称
            String filedType = fields[i].getType().toString(); //属性类型
            //去掉前面的class啊inteface啊这些说明
            filedType = filedType.substring(filedType.indexOf(" ")+1);
            //如果是Set类型的
            if(filedType.indexOf("Set")!=-1){
                String firstLetter = fieldName.substring(0, 1).toUpperCase();
                //得到方法名称
                String method = "get" + firstLetter + fieldName.substring(1);
                try {
                    //先调用自身的方法
                    Method callMethod = object.getClass().getMethod(method, new Class[] {});
                    //调用set的size用于实例化
                    Set set = (Set)callMethod.invoke(object, new Object[] {});
                    set.size();
                } catch (Exception e) {
                    try {
                        //上面调用自身的方法如果调不到会抛异常，这里再调用父类的方法
                        Method callMethod = object.getClass().getSuperclass().getDeclaredMethod(method,new Class[] {});
                        Set set = (Set) callMethod.invoke(object, new Object[] {});
                        set.size();
                    } catch (Exception e1) {
                        System.out.println(e1.getMessage());
                    }
                }
            }
        }*/
    }
}
