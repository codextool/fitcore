package top.topicsky.www.fitcore.global.util;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Random;

/**
 * The type Util image.
 */
@Component
@Transactional
public class UtilImage implements Serializable{
    /**
     * Get random char char [ ].
     *
     * @return the char [ ]
     */
    public static char[] getRandomChar(){
        char[] codes=new char[]{'1','2','3','4','5','6','7','8',
                '9','0','A','B','C','D','E','F','G','H','I','J','K'};
        char[] sj=new char[4];
        for(int i=0;i<sj.length;i++){
            sj[i]=codes[new Random().nextInt(21)];
        }
        System.out.println(new String(sj));
        return sj;
    }

    /**
     * Gets image.
     *
     * @param codes
     *         the codes
     *
     * @return the image
     */
    public static BufferedImage getImage(char[] codes){
        BufferedImage buffer=new BufferedImage(80,30,BufferedImage.TYPE_INT_RGB);
        Graphics graphics=buffer.getGraphics();
        graphics.setColor(Color.PINK);
        graphics.drawRect(0,0,80,40);
        graphics.setColor(Color.WHITE);
        graphics.drawString(codes[0]+"",8,12);
        graphics.drawString(codes[1]+"",22,25);
        graphics.drawString(codes[2]+"",48,18);
        graphics.drawString(codes[3]+"",65,28);
        graphics.dispose();
        return buffer;
    }
}
