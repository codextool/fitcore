package top.topicsky.www.fitcore.global.entity;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * 使用 @Data 相当于同时使用了@ToString、@EqualsAndHashCode、@Getter、@Setter和@RequiredArgsConstructor这些注解
 *
 * @Cleanup 他可以帮我们在需要释放的资源位置自动加上释放代码
 * @Synchronized 可以帮我们在方法上添加同步代码块
 * @XmlRootElement 定义了注解注入实例
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString(callSuper=true)
/**
 * 这里如果不写false,会报错,
 * Error:(21, 1) 警告: Generating equals/hashCode implementation but without a call to superclass,
 * even though this class does not extend java.lang.Object. If this is intentional,
 * add '@EqualsAndHashCode(callSuper=false)' to your type.
 */
@EqualsAndHashCode(callSuper=false)
@RequiredArgsConstructor
@AllArgsConstructor
@Slf4j
@Repository
@Transactional
@XmlRootElement
public class MycodeEntity implements Serializable{
    private static final long serialVersionUID=-2929441467093015820L;
    /**
     *
     **/
    private Long myCodeId;
    /**
     *
     **/
    private String myCodeName;
    /**
     *
     **/
    private String myCodeAge;
    /**
     *
     **/
    private String myCodeIphone;
    /**
     *
     **/
    private String muCodeAddress;
}
