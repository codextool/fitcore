package top.topicsky.www.fitcore.global.cache.redis.advanced.utilize;

/**
 * 所在项目名称 ： fitcorebate
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.cache.redis.advanced.utilize
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 21 日 17 时 35 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreAdvancedMainImpl{
    public static void main(String[] args){
        for(int i=0;i<10;i++){
            FitCoreAdvancedReentrantLockClientThreadImpl outReentrantLock=new FitCoreAdvancedReentrantLockClientThreadImpl(i);
            //FitCoreAdvancedSynchronizedClientThreadImpl outSynchronized=new FitCoreAdvancedSynchronizedClientThreadImpl(i);
            outReentrantLock.start();
            //outSynchronized.start();
        }
    }
}
