package top.topicsky.www.fitcore.procedure.control.impl;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import top.topicsky.www.fitcore.global.dto.FitCoreOnlineFormDto;
import top.topicsky.www.fitcore.procedure.control.inter.FitCoreOnlineInter;
import top.topicsky.www.fitcore.procedure.control.predicate.FitCoreOnlinePredicateImpl;
import top.topicsky.www.fitcore.procedure.control.process.FitCoreOnlineProcessImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 11 时 17 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
@RequestMapping(value="/Online")
public class FitCoreOnlineImpl implements FitCoreOnlineInter, Serializable{
    /**
     * Online init string.
     *
     * @param Init
     *         the init string
     * @param model
     *         the model
     *
     * @return the string
     */
    @RequestMapping(value="/{Init}", method=RequestMethod.GET)
    public String onlineInit(@PathVariable String Init,Model model){
        return FitCoreOnlineProcessImpl.FitCoreOnlineProcessInit(
                FitCoreOnlineProcessImpl.FitCoreOnlineProcessInitNodel(Init,
                        model,
                        FitCoreOnlinePredicateImpl::FitCoreOnlineProcessInitNodel),
                FitCoreOnlinePredicateImpl::FitCoreOnlinePredicateInit
        );
    }

    @RequestMapping(value="/{Process}", method=RequestMethod.POST)
    public String onlineProcess(@ModelAttribute @PathVariable FitCoreOnlineFormDto fitCoreOnlineFormDto,
                                String Process,
                                Model model,
                                HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse){
        FitCoreOnlineProcessImpl.FitCoreOnlineProcessInitProcessCheck(fitCoreOnlineFormDto,
                Process,
                model,
                FitCoreOnlineProcessImpl.FitCoreOnlineProcessInitProcess(fitCoreOnlineFormDto,
                        httpServletRequest,
                        httpServletResponse,
                        FitCoreOnlinePredicateImpl::FitCoreOnlinePredicateInitProcess)
        );
        //
        //if(fitCoreOnlineFormDto.getUsername() != null
        //        && fitCoreOnlineFormDto.getUsername()!=""
        //        &&fitCoreOnlineFormDto.getPassword()!=null
        //        &&fitCoreOnlineFormDto.getPassword()!=""){
        //
        //    return Process;
        //}
        return "error/page_500";
    }
}
