package top.topicsky.www.fitcore.procedure.dao.annotation;

import org.apache.ibatis.annotations.*;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;

import java.io.Serializable;
import java.util.List;

import static org.apache.ibatis.type.JdbcType.BIGINT;
import static org.apache.ibatis.type.JdbcType.VARCHAR;
import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.dao.FitCoreDaoEternalPublicUserEntityDao.*;

/**
 * The interface Fit core user entity dao.
 */
@Transactional
@CacheNamespace(size=512)
public interface FitCoreAnnotationUserEntityDao extends Serializable{
    /**
     * Insert int.
     *
     * @param fitCoreUserEntity
     *         the fit core user entity
     *
     * @return the int
     */
    @Insert(INSERTCONDITION)
    @Options(
            /*开启缓存*/
            useCache=true,
            /*响应时间*/
            timeout=5000,
            /*自增主键*/
            useGeneratedKeys=true,
            /*自增主键写入数据库对应字段*/
            keyProperty="myCodeId",
            /*写入后刷新缓存*/
            flushCache=true
    )
    int insert(FitCoreUserEntity fitCoreUserEntity);

    /**
     * Delete.
     *
     * @param fitCoreId
     *         the fit core id
     */
    @Delete(DELETECONDITION)
    @Options(
            /*开启缓存*/
            useCache=true,
            /*响应时间*/
            timeout=5000,
            /*写入后刷新缓存*/
            flushCache=true
    )
    void delete(@Param("fitCoreId") Long fitCoreId);

    /**
     * Update.
     *
     * @param fitCoreUserEntity
     *         the fit core user entity
     */
    @Update(UPDATECONDITION)
    @Options(
            /*开启缓存*/
            useCache=true,
            /*响应时间*/
            timeout=5000,
            /*写入后刷新缓存*/
            flushCache=true
    )
    void update(FitCoreUserEntity fitCoreUserEntity);

    /**
     * Select fit core user entity.
     *
     * @param fitCoreId
     *         the fit core id
     *
     * @return the fit core user entity
     */
    @Select(SELECTCONDITION)
    @Results({
            @Result(id=true, column="fitCoreId", property="fitCoreId", jdbcType=BIGINT, javaType=Long.class),
            @Result(column="fitCoreUuid", property="fitCoreUuid", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreImage", property="fitCoreImage", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreTheme", property="fitCoreTheme", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreCname", property="fitCoreUuid", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreEname", property="fitCoreEname", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreDisplayname", property="fitCoreDisplayname", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreAge", property="fitCoreAge", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreBirthday", property="fitCoreBirthday", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreEmail", property="fitCoreUuid", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCorePassword", property="fitCorePassword", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCorePhone", property="fitCorePhone", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreTelphone", property="fitCoreTelphone", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreCountry", property="fitCoreCountry", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreProvince", property="fitCoreProvince", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreCity", property="fitCoreCity", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreStreet", property="fitCoreStreet", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreCompany", property="fitCoreCompany", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreDepartment", property="fitCoreDepartment", jdbcType=VARCHAR, javaType=String.class),
            @Result(column="fitCoreIp", property="fitCoreIp", jdbcType=VARCHAR, javaType=String.class)
    })
    @Options(
            /*开启缓存*/
            useCache=true,
            /*响应时间*/
            timeout=5000,
            /*写入后刷新缓存*/
            flushCache=true
    )
    FitCoreUserEntity select(@Param("fitCoreId") Long fitCoreId);

    /**
     * Select all list.
     *
     * @return the list
     */
    @Select(SELECTALLCONDITION)
    @Options(
            /*开启缓存*/
            useCache=true,
            /*响应时间*/
            timeout=5000,
            /*写入后刷新缓存*/
            flushCache=true
    )
    List<FitCoreUserEntity> selectAll();
}
