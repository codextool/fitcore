package top.topicsky.www.fitcore.procedure.dao.core;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 15 日 09 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@Repository
@Transactional
public abstract class FitCoreDaoBaseImpl<SI,RT> extends FitCoreDaoImpl implements FitCoreDaoBaseInter, Serializable{
    @Override
    public Object fitCoreDaoInit(Object o){
        return super.fitCoreDaoInit(o);
    }

    @Override
    public Object fitCoreControlInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreBaseInit(Object o){
        return super.fitCoreBaseInit(o);
    }

    @Override
    public Object fitCoreAdapterInit(Object o){
        return super.fitCoreAdapterInit(o);
    }

    @Override
    public Object fitCoreControlBaseInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreControlAdapterInit(Object o){
        return super.fitCoreControlAdapterInit(o);
    }

    @Override
    public Object fitCoreServiceInit(Object o){
        return super.fitCoreServiceInit(o);
    }

    @Override
    public Object fitCoreServiceBaseInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreServiceAdapterInit(Object o){
        return super.fitCoreServiceAdapterInit(o);
    }

    @Override
    public Object fitCoreDaoBaseInit(Object o){
        return null;
    }
}
