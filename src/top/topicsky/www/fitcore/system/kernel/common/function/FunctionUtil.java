package top.topicsky.www.fitcore.system.kernel.common.function;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.function.Function;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.common.function
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 26 日 17 时 16 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@Component
@Transactional
public interface FunctionUtil<SI,RT> extends Serializable{
    /**
     * The Ots.
     */
    Function<Object,String> OTS=Object::toString;
    /**
     * The Sti.
     */
    Function<String,Integer> STI=Integer::valueOf;
    /**
     * The Its.
     */
    Function<Integer,String> ITS=String::valueOf;
    /**
     * The constant GBEAN.
     */
    Function<String,Object> GBEAN=(SI)->{
        //String user = "org.entity.User";//字符串是该类的全限定名
        Object classObj=null;
        try{
            Class clzz=Class.forName(SI);
            classObj=clzz.newInstance();//将class类转换为对象
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();
        }
        return classObj;
    };
    /**
     * The constant STCHAR.
     */
    Function<String,char[]> STCHAR=(string)->{
        char[] cs=string.toCharArray();
        cs[0]-=32;
        return cs;
    };
}
