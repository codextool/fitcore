package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.mediator;

import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.mediator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 11 时 52 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Transactional
public class FitCoreBasicObjectInheritXBImpl extends FitCoreBasicObjectAbstractImpl implements Serializable{
    /**
     * Instantiates a new Fit core basic object inherit xb.
     *
     * @param fitCoreBasicMediator
     *         the fit core basic mediator
     */
    public FitCoreBasicObjectInheritXBImpl(FitCoreBasicMediatorInter fitCoreBasicMediator){
        super(fitCoreBasicMediator);
        SOPL.accept("这里构造——XB——类结束，继承自控制抽象类");
    }

    @Override
    public void takeEffect(){
        SOPL.accept("这里是被调用的实际对象，这里调用了XB类方法");
    }
}
