package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.state.state;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.TransactionAttribute;
import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.state.state
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 10 时 53 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@TransactionAttribute
public class FitCoreBasicAtateSwitchImpl<SI> implements Serializable{
    private FitCoreBasicStateCoreImpl fitCoreBasicStateCore;

    /**
     * Instantiates a new Fit core basic atate switch.
     *
     * @param fitCoreBasicStateCore
     *         the fit core basic state core
     */
    public FitCoreBasicAtateSwitchImpl(FitCoreBasicStateCoreImpl fitCoreBasicStateCore){
        SOPL.accept("构造状态实际需要联动操作业务类（*Core）");
        this.fitCoreBasicStateCore=fitCoreBasicStateCore;
    }

    /**
     * Fit core basic atate switch.
     */
    public void fitCoreBasicAtateSwitch(){
        if(fitCoreBasicStateCore.getSi().equals("在线")){
            SOPL.accept("这里将切换到——在线——方式之前");
            fitCoreBasicStateCore.FitCoreBasicAtateSwitchOnline();
            SOPL.accept("这里将切换到——在线——方式之后");
        }else if(fitCoreBasicStateCore.getSi().equals("离线")){
            SOPL.accept("这里将切换到——离线——方式之前");
            fitCoreBasicStateCore.FitCoreBasicAtateSwitchOutline();
            SOPL.accept("这里将切换到——离线——方式之后");
        }
    }
}
