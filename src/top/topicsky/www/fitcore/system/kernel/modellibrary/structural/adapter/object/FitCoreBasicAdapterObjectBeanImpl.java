package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.adapter.object;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.adapter.object
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 09 时 54 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicAdapterObjectBeanImpl implements FitCoreBasicAdapterObjectInter{
    private FitCoreBasicAdapterObjectImpl fitCoreBasicAdapterObjectImpl;

    /**
     * Instantiates a new Fit core basic adapter object bean.
     *
     * @param fitCoreBasicAdapterObjectImpl
     *         the fit core basic adapter object
     */
    public FitCoreBasicAdapterObjectBeanImpl(FitCoreBasicAdapterObjectImpl fitCoreBasicAdapterObjectImpl){
        super();
        this.fitCoreBasicAdapterObjectImpl=fitCoreBasicAdapterObjectImpl;
    }

    @Override
    public void fitCoreBasicAdapterObjectInherit(){
        fitCoreBasicAdapterObjectImpl.fitCoreBasicAdapterObjectInherit();
    }

    @Override
    public void fitCoreBasicAdapterObjectInter(){
        SOPL.accept("这里是自主的方法调用");
    }
}
