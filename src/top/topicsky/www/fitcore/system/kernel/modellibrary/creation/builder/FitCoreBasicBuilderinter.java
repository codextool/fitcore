package top.topicsky.www.fitcore.system.kernel.modellibrary.creation.builder;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.creation.builder
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 17 时 06 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@FunctionalInterface
public interface FitCoreBasicBuilderinter<SI,RT> extends Serializable{
    /**
     * Builder init rt.
     *
     * @param si
     *         the si
     *
     * @return the rt
     */
    RT builderInit(SI si);
}
