package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.composite;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.composite
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 17 时 03 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreBasicCompositeBeanImpl implements Serializable{
    /**
     * The Root.
     */
    FitCoreBasicCompositeImpl root=null;

    /**
     * Instantiates a new Fit core basic composite bean.
     *
     * @param name
     *         the name
     */
    public FitCoreBasicCompositeBeanImpl(String name){
        root=new FitCoreBasicCompositeImpl(name);
    }

    /**
     * The entry point of application.
     *
     * @param args
     *         the input arguments
     */
    public static void main(String[] args){
        FitCoreBasicCompositeBeanImpl bean=new FitCoreBasicCompositeBeanImpl("A");
        FitCoreBasicCompositeImpl nodeB=new FitCoreBasicCompositeImpl("B");
        FitCoreBasicCompositeImpl nodeC=new FitCoreBasicCompositeImpl("C");
        nodeB.add(nodeC);
        bean.root.add(nodeB);
        SOPL.accept("这里测试树形建设集合，测试组合模式");
    }
}
