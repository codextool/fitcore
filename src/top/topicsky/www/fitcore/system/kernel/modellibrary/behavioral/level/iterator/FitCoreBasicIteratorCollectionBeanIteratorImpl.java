package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.iterator;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.level.iterator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 15 时 14 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreBasicIteratorCollectionBeanIteratorImpl implements FitCoreBasicIteratorCollectionIteratorInter{
    private FitCoreBasicIteratorCollectionBeanImpl fitCoreBasicIteratorCollectionBeanImpl;
    private int pos=-1;

    /**
     * Instantiates a new Fit core basic iterator collection bean iterator.
     *
     * @param fitCoreBasicIteratorCollectionBeanImpl
     *         the fit core basic iterator collection bean
     */
    public FitCoreBasicIteratorCollectionBeanIteratorImpl(FitCoreBasicIteratorCollectionBeanImpl fitCoreBasicIteratorCollectionBeanImpl){
        this.fitCoreBasicIteratorCollectionBeanImpl=fitCoreBasicIteratorCollectionBeanImpl;
    }

    @Override
    public Object obtainPrevious(){
        if(pos>0){
            pos--;
        }
        return fitCoreBasicIteratorCollectionBeanImpl.obtainElement(pos);
    }

    @Override
    public Object obtainNext(){
        if(pos<fitCoreBasicIteratorCollectionBeanImpl.obtainSize()-1){
            pos++;
        }
        return fitCoreBasicIteratorCollectionBeanImpl.obtainElement(pos);
    }

    @Override
    public boolean hasNext(){
        if(pos<fitCoreBasicIteratorCollectionBeanImpl.obtainSize()-1){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Object obtainFirst(){
        pos=0;
        return fitCoreBasicIteratorCollectionBeanImpl.obtainElement(pos);
    }
}
