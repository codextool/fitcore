package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.flyweight;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.flyweight
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 29 日 09 时 45 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicFlyWeightImpl implements Serializable{
    private Vector<Connection> pool;
    /*公有属性*/
    private String url="jdbc:mysql://localhost:3306/test";
    private String username="test";
    private String password="root";
    private String driverClassName="com.mysql.jdbc.Driver";
    /*连接池设定最大连接数*/
    private int poolSize=100;

    /*构造方法，做一些初始化工作*/
    private FitCoreBasicFlyWeightImpl(){
        Connection connConstructor=null;
        pool=new Vector<Connection>(poolSize);
        for(int i=0;i<poolSize;i++){
            try{
                Class.forName(driverClassName);
                connConstructor=DriverManager.getConnection(url,username,password);
                pool.add(connConstructor);
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Repatriate.
     *
     * @param connRepatriate
     *         the conn repatriate
     */
/* 返回连接到连接池 */
    public synchronized void repatriate(Connection connRepatriate){
        SOPL.accept("归还连接池前动作");
        pool.add(connRepatriate);
        SOPL.accept("归还连接池后动作，并且显示连接池现存连接数量"+pool.size());
    }

    /**
     * Get connection connection.
     *
     * @return the connection
     */
/* 返回连接池中的一个数据库连接 */
    public synchronized Connection getConnection(){
        if(pool.size()>0){
            Connection connreturn=pool.get(0);
            SOPL.accept("取得之前连接池现存连接数量"+pool.size());
            pool.remove(connreturn);
            SOPL.accept("取得之后连接池现存连接数量"+pool.size());
            return connreturn;
        }else{
            SOPL.accept("连接池没有可用连接，请清理");
            return null;
        }
    }
}
