package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCVariablePublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCVariableModelPublic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.STCHAR;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FCVariableProcessPublic implements Serializable{
    /**
     * The Fc variable model public golbal.
     */
    static Supplier<FCVariableModelPublic> fcVariableModelPublicGolbal=()->new FCVariableModelPublic();
    /**
     * The Clazz fc variable public.
     */
    static Class<FCVariablePublic> clazzFCVariablePublic=FCVariablePublic.class;

    /**
     * Gets fc variable public info.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fc variable public info
     *
     * @throws Exception
     *         the exception
     */
    public static FCVariableModelPublic getFCVariablePublicInfo(Class<?> clazz) throws Exception{
        FCVariableModelPublic fcVariableModelPublic=fcVariableModelPublicGolbal.get();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FCVariablePublic annotationFCVariablePublicField=field.getAnnotation(clazzFCVariablePublic);
            boolean annotationFCVariablePublicBoolean=field.isAnnotationPresent(clazzFCVariablePublic);
            Class fcVariableModelPublicClazz=fcVariableModelPublic.getClass();
            Method m1=fcVariableModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCVariablePublicBoolean){
                m1.invoke(fcVariableModelPublic,annotationFCVariablePublicField.variableExp());
            }
        }
        return fcVariableModelPublic;
    }

    /**
     * Gets fc variable public info object.
     *
     * @param object
     *         the object
     *
     * @return the fc variable public info object
     *
     * @throws Exception
     *         the exception
     */
    public static FCVariableModelPublic getFCVariablePublicInfoObject(Object object) throws Exception{
        FCVariableModelPublic fcVariableModelPublic=fcVariableModelPublicGolbal.get();
        Field[] fields=object.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            Object filedstemp=field.get(object);
            FCVariablePublic annotationFCVariablePublicField=field.getAnnotation(clazzFCVariablePublic);
            boolean annotationFCVariablePublicBoolean=field.isAnnotationPresent(clazzFCVariablePublic);
            Class fcVariableModelPublicClazz=fcVariableModelPublic.getClass();
            Method m1=fcVariableModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCVariablePublicBoolean){
                if(filedstemp!=null){
                    m1.invoke(fcVariableModelPublic,OTS.apply(filedstemp));
                }else{
                    m1.invoke(fcVariableModelPublic,annotationFCVariablePublicField.variableExp());
                }
            }else{
                if(filedstemp!=null){
                    m1.invoke(fcVariableModelPublic,OTS.apply(filedstemp));
                }
            }
        }
        return fcVariableModelPublic;
    }
}
