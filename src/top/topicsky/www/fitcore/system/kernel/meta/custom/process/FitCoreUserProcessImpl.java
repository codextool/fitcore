package top.topicsky.www.fitcore.system.kernel.meta.custom.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.custom.core.FitCoreKeyMate;
import top.topicsky.www.fitcore.system.kernel.meta.custom.core.FitCoreNotRecordMate;
import top.topicsky.www.fitcore.system.kernel.meta.custom.core.FitCoreTableMate;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.custom.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 14 时 11 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreUserProcessImpl implements Serializable{
    /**
     * 通过实体类生成 insert into sql语句
     *
     * @param cl
     *         the cl
     *
     * @return string string
     *
     * @throws Exception
     *         the exception
     */
    public static String save(Object cl) throws Exception{
        String sql="insert into ";
        if(cl!=null){
            Field[] fiels=cl.getClass().getDeclaredFields();//获得反射对象集合
            boolean t=cl.getClass().isAnnotationPresent(FitCoreTableMate.class);//获得类是否有注解
            if(t){
                FitCoreTableMate tab=cl.getClass().getAnnotation(FitCoreTableMate.class);
                sql+=tab.name();//获得表名
                String name="";//记录字段名
                String value="";//记录值名称
                boolean bl=false;//记录主键是否为空
                for(Field fl : fiels){//循环组装
                    fl.setAccessible(true);//开启支私有变量的访问权限
                    Object tobj=fl.get(cl);
                    if(tobj!=null){
                        if(fl.isAnnotationPresent(FitCoreKeyMate.class)){//判断是否存在主键
                            bl=true;
                        }
                        if(!fl.isAnnotationPresent(FitCoreNotRecordMate.class)){
                            name+=fl.getName()+",";
                            value+="'"+tobj.toString()+"',";
                        }
                    }
                }
                if(bl){
                    if(name.length()>0)
                        name=name.substring(0,name.length()-1);
                    if(value.length()>0)
                        value=value.substring(0,value.length()-1);
                    sql+="("+name+") values("+value+")";
                }else
                    throw new Exception("未找到类主键 主键不能为空");
            }else
                throw new Exception("传入对象不是实体类");
        }else
            throw new Exception("传入对象不能为空");//抛出异常
        return sql;
    }

    /**
     * 传入对象更新
     *
     * @param obj
     *         the obj
     *
     * @return string string
     *
     * @throws Exception
     *         the exception
     */
    public static String update(Object obj) throws Exception{
        String sql="update ";
        if(obj!=null){
            Field[] fiels=obj.getClass().getDeclaredFields();//获得反射对象集合
            boolean t=obj.getClass().isAnnotationPresent(FitCoreTableMate.class);//获得类是否有注解
            if(t){
                FitCoreTableMate tab=obj.getClass().getAnnotation(FitCoreTableMate.class);
                sql+=tab.name()+" set ";//获得表名
                String wh="";//记录字段名
                String k="";
                boolean bl=false;//记录主键是否为空
                for(Field fl : fiels){//循环组装
                    fl.setAccessible(true);//开启支私有变量的访问权限
                    Object tobj=fl.get(obj);
                    if(tobj!=null){
                        if(fl.isAnnotationPresent(FitCoreKeyMate.class)){//判断是否存在主键
                            bl=true;
                            k=fl.getName()+"='"+tobj.toString()+"' where  ";
                        }else{
                            if(!fl.isAnnotationPresent(FitCoreNotRecordMate.class)){
                                wh+=fl.getName()+"='"+tobj.toString()+"',";
                            }
                        }
                    }
                }
                if(bl){
                    if(wh.length()>0)
                        wh=wh.substring(0,wh.length()-1);
                    if(k.length()>0)
                        k=k.substring(0,k.length()-1);
                    sql+=k+wh;
                }else
                    throw new Exception("未找到类主键 主键不能为空");
            }else
                throw new Exception("传入对象不是实体类");
        }else
            throw new Exception("传入对象不能为空");//抛出异常
        return sql;
    }
}
