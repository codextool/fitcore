package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCLayoutPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCLayoutModelPublic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.STCHAR;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FCLayoutProcessPublic implements Serializable{
    /**
     * The Fc layout model public golbal.
     */
    static Supplier<FCLayoutModelPublic> fcLayoutModelPublicGolbal=()->new FCLayoutModelPublic();
    /**
     * The Clazz fc layout public.
     */
    static Class<FCLayoutPublic> clazzFCLayoutPublic=FCLayoutPublic.class;

    /**
     * Gets fc layout public info.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fc layout public info
     *
     * @throws Exception
     *         the exception
     */
    public static FCLayoutModelPublic getFCLayoutPublicInfo(Class<?> clazz) throws Exception{
        FCLayoutModelPublic fcLayoutModelPublic=fcLayoutModelPublicGolbal.get();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FCLayoutPublic annotationFCLayoutPublicField=field.getAnnotation(clazzFCLayoutPublic);
            boolean annotationFCLayoutPublicBoolean=field.isAnnotationPresent(clazzFCLayoutPublic);
            Class fcLayoutModelPublicClazz=fcLayoutModelPublic.getClass();
            Method m1=fcLayoutModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCLayoutPublicBoolean){
                m1.invoke(fcLayoutModelPublic,annotationFCLayoutPublicField.layoutExp());
            }
        }
        return fcLayoutModelPublic;
    }

    /**
     * Gets fc layout public info object.
     *
     * @param object
     *         the object
     *
     * @return the fc layout public info object
     *
     * @throws Exception
     *         the exception
     */
    public static FCLayoutModelPublic getFCLayoutPublicInfoObject(Object object) throws Exception{
        FCLayoutModelPublic fcLayoutModelPublic=fcLayoutModelPublicGolbal.get();
        Field[] fields=object.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            Object filedstemp=field.get(object);
            FCLayoutPublic annotationFCLayoutPublicField=field.getAnnotation(clazzFCLayoutPublic);
            boolean annotationFCLayoutPublicBoolean=field.isAnnotationPresent(clazzFCLayoutPublic);
            Class fcLayoutModelPublicClazz=fcLayoutModelPublic.getClass();
            Method m1=fcLayoutModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCLayoutPublicBoolean){
                if(filedstemp!=null){
                    m1.invoke(fcLayoutModelPublic,OTS.apply(filedstemp));
                }else{
                    m1.invoke(fcLayoutModelPublic,annotationFCLayoutPublicField.layoutExp());
                }
            }else{
                if(filedstemp!=null){
                    m1.invoke(fcLayoutModelPublic,OTS.apply(filedstemp));
                }
            }
        }
        return fcLayoutModelPublic;
    }
}
